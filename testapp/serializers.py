from testapp.models import *
from rest_framework.serializers import ModelSerializer
class UserSerializer(ModelSerializer):
    class Meta:
        model=User 
        fields=("id","is_deleted","created_at","username","first_name","middle_name","last_name","email","is_superuser",'is_staff','is_email_verified','is_active','full_name','email','groups','role')
