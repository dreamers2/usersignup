import re 
def check_password_validation(password):
    """ This function will create a strong Password as
    combination of ablest 1 Capital letter ,1 Numbers and 1 Special character"""
    pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!#$%^&*(),.-_+=<>?])[A-Za-z\d!#$%^&*(),.-_+=<>?]{6,12}$"
    return False if len(re.findall(pattern, password)) == 0 else True
