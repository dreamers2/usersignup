from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from testapp.enums import RolesEnum
class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            for group in RolesEnum:
                new_group, created = Group.objects.get_or_create(name=group.value)
                self.stdout.write(self.style.SUCCESS('Successfully created role "%s"' % new_group.name))

        except Exception as error:
            CommandError('Error in Command in create group',error)


