from testapp.models import *
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError
from django.conf import settings
from rest_framework.views import APIView
from testapp.services import *
from testapp.serializers import *
from testapp.mixins import *
from rest_framework.permissions import IsAdminUser,IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import LimitOffsetPagination
from django.contrib.auth import login,logout,authenticate
from testapp.permissions import *
class SignUp(CreateListMixin,ModelViewSet):
    '''
    API for Registration of Vendor and Bidder, 
    Editing Their Profile,Getting there Profile,Deleting there Profile  
    requested_payload={
        "first_name": "",
        "last_name": "",
        "email": "",(unique)
        "Password":""
        "username":""(unique) 
    }
    NOTE: THIS API REGISTERS THE USER AS WELL AS ASSIGN THE USER
    DIFFERENT ROLES, FOR ASSIGNING ROLES ONE SHOULD MUST HAVE 
    ADMIN ACCESS, By hiting this API you can create multiple user
    in one sort
    '''
    queryset=User.objects.all()
    serializer_class=UserSerializer
    pagination_class=LimitOffsetPagination
    def create(self,request):
        '''
        Adding Custom Create Function to Set the user password
        while registering 
        '''
        try: 
            params=request.data
            try: 
                password=params.pop("password")
            except:
                password=None   
            if not password:
                return Response({'message':"OOPS, Password Not Found"},status=status.HTTP_400_BAD_REQUEST)
            params.update(password=password)
            if not check_password_validation(params['password'].strip()):
                return Response({"message": "Enter the right format of the password"},status=status.HTTP_400_BAD_REQUEST)
            params['groups'] = Group.objects.get(name="USER").id
            try:
                serializer=UserSerializer(data=params)
                if serializer.is_valid(raise_exception=True):
                    user=serializer.save() 
                    user.set_password(params['password'])
                    user.save()
                    return Response({'message':'Successfully Registered',"Data":serializer.data},status=status.HTTP_201_CREATED)
            except:
                return Response({'message':'Something went wrong while Registering',"errors":serializer.errors},status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({'message':'Something went wrong while SignUp'},status=status.HTTP_500_INTERNAL_SERVER_ERROR) 
    @action(detail=True,methods=['put','patch'],permission_classes=(IsAuthenticated,IsAdminUser))
    def assign_roles(self,request,pk=None):
        '''
        requested_payload:{
            "username": "",
            "role": ""--------> valid choice ["VENDOR","BIDDER"]
        Accessed by : Only Admin 
        Only Admin Can assign the role 
        }
        '''
        try: 
            queryset=User.objects.all()
            params=request.data
            try: 
                username=params.pop("username") 
                
            except:
                username=None  
            try: 
                role=params.pop("role") 
            except: 
                role=None 
            if not username:
                return Response({'message':"UserName Not Found"},status=status.HTTP_400_BAD_REQUEST)
            if not role: 
                return Response({'message':"OOPS,Role Not Found"},status=status.HTTP_400_BAD_REQUEST)
            params.update(username=username)
            params.update(role=role)
            if role == 'VENDOR' or role == 'BIDDER':
                pass 
            else: 
                return Response({"message":"OOPS, Not a valid Role, Valid Role can be Only 'VENDOR' OR 'BIDDER'"},status=status.HTTP_400_BAD_REQUEST) 
            try: 
                user=get_object_or_404(queryset,pk=pk)
                user.role=role 
                user.save()
                return Response({"message":"Roles Successfully assigned to the requested User"},status=status.HTTP_200_OK) 
            except:
                return Response({'message':"OOPS, Requested User Not Found"},status=status.HTTP_404_NOT_FOUND) 
        except:
            return Response({'message':'Something went wrong Assigning Roles'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
class UserLogin(APIView):
    def post(self,request):
        try: 
            params=request.data 
            try: 
                user_exist = User.objects.get(username=(params['username']).lower())
            except Exception as err:
                return Response({"message": "This username does not exists."}, status=status.HTTP_400_BAD_REQUEST) 
            if user_exist.is_active == False:
                return Response({"message": "Your account is disabled/suspended by the Admin. "},status=status.HTTP_400_BAD_REQUEST)
            if user_exist.is_deleted:
                return Response({"message": "Your account is Deleted by the Admin."},status=status.HTTP_400_BAD_REQUEST)
            try:
                user = authenticate(request, username=params['username'].lower(), password=params['password'])
                login(request, user)
                user.save()
            except Exception as e:
                return Response({"message": "You enter the wrong credentials."}, status=status.HTTP_400_BAD_REQUEST)
            return Response({"message": "Logged in successfully."},status=status.HTTP_200_OK)
        except:
            return Response({'message':'Something Went Wrong While Logging'},status=status.HTTP_500_INTERNAL_SERVER_ERROR) 

