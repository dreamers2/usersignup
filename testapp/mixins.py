class CreateListMixin:
    '''
    mixins to bulk create 
    '''
    def get_serializer(self,*args,**kwargs):
        if isinstance(kwargs.get("data",{}),list):
            kwargs['many']=True 
        return super().get_serializer(*args,**kwargs)