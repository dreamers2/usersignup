from django.contrib import admin
from testapp.models import User 
class UserAdmin(admin.ModelAdmin):
    search_fields=['username','email','created_at']
    list_display=["id","is_deleted","created_at",'groups','username','first_name','middle_name','email','is_superuser','is_staff','is_active','is_email_verified','full_name','last_name','role']
admin.site.register(User,UserAdmin)    