# Generated by Django 3.0.6 on 2020-10-31 16:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import testapp.enums


class Migration(migrations.Migration):

    dependencies = [
        ('testapp', '0003_user_is_deleted'),
    ]

    operations = [
        migrations.CreateModel(
            name='Token',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token_type', models.CharField(choices=[(testapp.enums.TokenType['EMAIL_VERIFICATION'], 'EMAIL VERIFICATION'), (testapp.enums.TokenType['EMAIL_VERIFICATION_WITH_PASSWORD'], 'EMAIL VERIFICATION WITH PASSWORD'), (testapp.enums.TokenType['RESET_PASSWORD'], 'RESET PASSWORD'), (testapp.enums.TokenType['FORGET_PASSWORD'], 'FORGET PASSWORD'), (testapp.enums.TokenType['OTP'], 'OTP'), (testapp.enums.TokenType['REFUND_OTP'], 'REFUND OTP'), (testapp.enums.TokenType['SEND_EMAIL'], 'SEND EMAIL')], max_length=100)),
                ('token', models.CharField(blank=True, default=None, max_length=100, null=True)),
                ('created_at', models.DateTimeField(blank=True, default=None, null=True, verbose_name='create date')),
                ('expiry_minutes', models.IntegerField(default=30)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Token',
            },
        ),
    ]
