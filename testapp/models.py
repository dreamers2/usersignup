import jwt
from testapp.enums import *
from django.db import models
from django.conf import settings 
from django.contrib.sessions.models import Session
from rest_framework_jwt.utils import jwt_payload_handler
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,Group
from django.contrib.auth.base_user import BaseUserManager
class UserManager(BaseUserManager):
    '''
    class extends BaseUserManager to Create superuser by Email 
    and password 
    '''
    def create_superuser(self,username,password,groups_id):
        user=self.model(username=username)
        user.set_password(password)
        user.is_superuser=True 
        user.is_staff=True 
        user.groups_id=groups_id
        user.is_active=True 
        user.is_email_verified=True
        user.save(using=self._db)
        return user  
class User(AbstractBaseUser,PermissionsMixin):
    groups=models.ForeignKey(Group,on_delete=models.CASCADE,related_name="user_set")
    first_name=models.CharField("User First Name",max_length=50,null=False,blank=False)
    middle_name=models.CharField("User Middle Name",max_length=50,null=True,blank=True)
    last_name=models.CharField("User Last Name",max_length=50,null=False,blank=False)
    username=models.CharField("UserName",max_length=50,null=False,blank=False,unique=True,error_messages={"unique":"OOPS,This UserName is Already Registered !!!!!!!!"})
    is_superuser=models.BooleanField("Is Superuser",default=False)
    is_staff=models.BooleanField("Is Staff",default=False)
    is_active=models.BooleanField("Is Active",default=False)
    is_email_verified=models.BooleanField("Is Email Verified",default=False)
    email=models.EmailField("User Email Address",null=False,blank=False,unique=True,error_messages={"unique":"This Email is Already Registered"})
    created_at=models.DateTimeField(auto_now_add=True)
    is_deleted=models.BooleanField("Is Deleted",default=False)
    role=models.CharField("Role Name",max_length=50,default="USER")
    objects=UserManager()
    USERNAME_FIELD="username"
    EMAIL_FIELD="email"
    REQUIRED_FIELDS = ['groups_id']
    full_name=models.CharField("User Full Name",max_length=150,null=True,blank=True)
    def get_full_name(self):
        if self.middle_name:
            return "{} {} {}".format(self.first_name,self.middle_name,self.last_name)
        return "{} {}".format(self.first_name,self.last_name)
    def save(self,*args,**kwargs):
        self.full_name=self.get_full_name()
        self.email=self.email.lower()
        self.username=self.username.lower()
        super(User,self).save(*args,**kwargs)
    