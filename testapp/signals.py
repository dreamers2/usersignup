from testapp.models import *
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.mail import send_mail
@receiver(post_save,sender=User) 
def signup_signal(sender,instance,created,**kwargs):
    '''
    signals monitors the user signup (registration)
    By using this signal we can send Activation link also ,if required
    '''
    if created:
        user_email=instance.email
        send_mail("testing","testing","testing",[user_email,],fail_silently=True)