from django.utils.deprecation import MiddlewareMixin
class DisableCsrf(MiddlewareMixin):
    '''
    custom middleware to disable csrf token
    '''
    def process_request(self,request):
        setattr(request,"_dont_enforce_csrf_check",True)