from django.contrib import admin
from django.urls import path,include
from testapp.routers import router

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include(router.urls)),
    path('api/', include('rest_framework.urls')),
    path("testapp/", include('testapp.urls')),

]
